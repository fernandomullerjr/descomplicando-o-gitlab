# Descomplicando o Gitlab

Treinamento ao vivo na Twitch - Descomplicando o Gitlab

### Day-1
```bash
- Entendemos o que é o Git.
- Aprendemos a diferença entre Working Dir, Index e HEAD.
- Entendemos o que é o Gitlab.
- Como criar um Grupo no Gitlab.
- Como criar um repositório no Git.
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git.
- Como criar uma Branch.
- Como criar um Merge Request.
- Como adicionar um membro no projeto.
- Como Fazer o merge na Master/Main.
```
